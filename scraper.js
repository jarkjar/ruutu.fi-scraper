//var request = require("request");
var request = require('sync-request');
var cheerio = require("cheerio");
var db = require("./models");

module.exports = {
    getJSON: function(scrapeUrl, ipAddress, timestamp) {
        console.log("scrapeUrl: " + scrapeUrl);
        console.log("ipAddress: " + ipAddress);
        console.log("timestamp: " + timestamp);

        var json = { mediaUrl: "", programName: "", description: "", imageUrl: "" };  
        var videoId = scrapeUrl.replace("http://www.ruutu.fi/video/", "").replace( /^\D+/g, '');
        if(videoId == null || videoId == "")
            return null;

        // Try to get "http://gatling.nelonenmedia.fi/media-xml-cache?id=xxxxxx"
        var gatlingUrl = "http://gatling.nelonenmedia.fi/media-xml-cache?id=" + videoId;
        var gatlingResponse = request("GET", gatlingUrl); 
            var gatlingData = gatlingResponse.getBody();
            if(gatlingData != null && gatlingData != "") {
                // mediaUrl = HTTPMediaFiles / HTTPMediaFile
                // programName = Behavior / Program@program_name
                // description = Behavior / Program@description
                // imageUrl = Behavior / StartPicture@href
                var $ = cheerio.load(gatlingData);
                json.mediaUrl = $("HTTPMediaFiles HTTPMediaFile").text();
                json.programName = $("Behavior Program").attr("program_name");
                json.description = $("Behavior Program").attr("description");
                json.imageUrl = $("Behavior StartPicture").attr("href");
                db.Scrape.create({
                        programName: json.programName,
                        description: json.description,
                        imageUrl: json.imageUrl,
                        mediaUrl: json.mediaUrl,
                        url: scrapeUrl,
                        ipAddress: ipAddress,
                        timestamp: timestamp
                    });
                return json;
            }
        return null;
    }
}