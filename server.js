var express = require("express");
var path = require("path");
var favicon = require("serve-favicon");
var errorhandler = require("errorhandler");
var models = require("./models");

var app = express();
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
var port = process.env.PORT || 3700;
app.set("port", port);

app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "bower_components")));
app.use(favicon(path.join(__dirname, "public", "images", "favicon.ico")));
app.use(require("./routes/routes"));

if ("development" === app.get("env")) {
    app.use(errorhandler());
}

models.sequelize.sync().then(function() {
    app.listen(port);
    console.log("Listening on port " + port);
});