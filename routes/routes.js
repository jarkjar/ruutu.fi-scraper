var express = require("express");
var router = express.Router();
var db = require("../models");

//var fs = require("fs");
var bodyParser = require("body-parser")
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var scraper = require("../scraper");

var getPopular = function(req, res) {
    var limit = 10;
    // programName, description, imageUrl, mediaUrl, url    
    db.sequelize.query("SELECT \"programName\", \"description\", \"imageUrl\", \"mediaUrl\", count(\"url\") AS \"count\", \"url\" FROM public.\"Scrapes\" GROUP BY \"url\", \"programName\", \"description\", \"imageUrl\", \"mediaUrl\" ORDER BY \"count\" DESC LIMIT " + limit)
        .then(function(data) {        

        var jsonData = [];
        for(var i=0; i<data[0].length; i++) {
            var jsonItem = {
                programName: data[0][i].programName,
                description: data[0][i].description,
                mediaUrl: data[0][i].mediaUrl,
                imageUrl: data[0][i].imageUrl,
                count: data[0][i].count, 
                url: data[0][i].url
            };
            jsonData.push(jsonItem);
        }

        res.send(jsonData);
    });
}

router.get("/", function(req, res) {    
    res.render("index", { message: "" });
});

router.post("/popular", getPopular);

router.post("/scrape", urlencodedParser, function(req, res) {
	var scrapeUrl = req.body.url;
    var ipAddress = req.connection.remoteAddress;
    var timestamp = new Date().getTime();
    var result = scraper.getJSON(scrapeUrl, ipAddress, timestamp);
    if(result == null)
    	res.render("index", { message: "Episode not found!" });	
    else 
    	res.render("result", { data: result });
});

module.exports = router;