"use strict";

module.exports = function(sequelize, DataTypes) {
  var Scrape = sequelize.define("Scrape", {
    programName: DataTypes.STRING,
    description: DataTypes.STRING(5000),
    imageUrl: DataTypes.STRING(500),
    mediaUrl: DataTypes.STRING(500),
    url: DataTypes.STRING(500),
    ipAddress: DataTypes.STRING,
    timestamp: DataTypes.DATE
  });

  return Scrape;
};